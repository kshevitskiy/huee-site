const FORMS = {

    diary (selector) {
        let $form = $(selector);
        let $successMsg = $(".publication-alert");
        $.validator.addMethod("letters", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z ]*$/);
        });

        $form.validate({
            rules: {
                name: {
                required: true,
                minlength: 3,
                letters: true
                },
                email: {
                required: true,
                email: true
                }
            },
            messages: {
                name: "Type your name",
                email: "Type your e-mail"
            },
            submitHandler: function(form) {

                let formEl = $(form);
                let formData = formEl.serialize();
      
                $.ajax({
                    type: 'POST',
                    url: formEl.attr('action'),
                    data: formData
                })
                .done(function(response) {
                    console.log(response);
                    $successMsg.show();
      
                    // Clear the form.
                    $('#name').val('');
                    $('#email').val('');
                    $('#phone').val('');
                    $('#form-comment').val('');
                })
                .fail(function(data) {
                    console.log('Error ', response);
                });
      
                return false;
            }
        });        
    },

    init () {
        FORMS.diary('form');
    }
}

export default {
    init: FORMS.init
}