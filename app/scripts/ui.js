const doc = window.document
const docEl = doc.documentElement
const fsToggle = document.getElementById('fsToggle');

const UI = {

	methods: {

		goFullScreen () {
			this.classList.remove('expanded');
			let requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen
			let cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen
		
			if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
			  requestFullScreen.call(docEl)
			  this.classList.add('expanded');
			} else {
			  cancelFullScreen.call(doc)
			}
		}
	},

	events () {
		fsToggle.addEventListener('click', UI.methods.goFullScreen, false);
	},

	init: function() {
		// UI.events();
	}
};

export default {
	init : UI.init
};
