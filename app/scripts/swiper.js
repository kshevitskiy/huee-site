import { Swiper, Navigation, Pagination, EffectFade, Controller } from 'swiper/dist/js/swiper.esm.js';
Swiper.use([Navigation, Pagination, EffectFade, Controller]);

const carousel = [...document.querySelectorAll('.carousel')];
const teamSliderEl = document.querySelector('.gallery-top');
const teamThumbsEl = document.querySelector('.gallery-thumbs');

const SWIPER = {

	slider (selector) {
        let whySwiper = new Swiper(selector, {
            effect: 'fade',
            fadeEffect: {
                crossFade: true
            },
            loop: true,
            pagination: {
                el: '.why-slider-pagination',
                clickable: true,
                renderBullet: function (index, className) {
                    return '<span class="' + className + '">' + 0 + (index + 1) + "." + '<\/span>';
                },
            },
            navigation: {
                nextEl: '.why-slider-button'
            },
        });
	},

	teamSlider (gallery, thumbs) {
		let gallerySlider = new Swiper(gallery, {
			effect: 'fade',
			fadeEffect: {
				crossFade: true
			},
			spaceBetween: 10,
			slidesPerView: 'auto',
			navigation: {
				nextEl: '.gallery-button-next',
				prevEl: '.gallery-button-prev',
			}
		});

		let galleryThumbs = new Swiper(thumbs, {
			spaceBetween: 10,
			slidesPerView: 3,
			navigation: {
				nextEl: '.thumbs-button-next',
				prevEl: '.thumbs-button-prev',
			}
		});		

        $('.gallery-top .close-button').on('click', close);
        $('.gallery-thumbs .swiper-slide').on('click', show);

        $('.gallery-thumbs .swiper-slide').each(function(index) {
            $(this).attr('data-id', index);
        });

        function show() {
            var id = $(this).attr('data-id');
            $('.gallery-top').toggleClass('active');
            setTimeout( function(){
                $('.gallery-thumbs').hide();
            }, 5);

            $('.HueeLogo, #fp-nav, #HueeNextBtn, #HueeBurger, #HueeCurrent').hide();
            gallerySlider.slideTo(id, 300, true);
        }

        function close() {
            $('.gallery-top').toggleClass('active');
            $('.gallery-thumbs').show();
            $('.HueeLogo, #fp-nav, #HueeNextBtn, #HueeBurger, #HueeCurrent').show();
        }		
	},

	carousel (selector) {
		selector.forEach(function(el, index) {
	        let swiper = new Swiper(el, {
	            grabCursor: true,
				slidesPerView: 'auto',				
				mousewheel: true,
				spaceBetween: 0,
				nested: true,
	        });
		});
	},

	methods: {
		goToFirstSlide (slider) {
			slider.slideTo(0, 1000);
		}
	},

	init: function() {
		(carousel[0] !== null) ? SWIPER.carousel(carousel) : '';
		SWIPER.teamSlider(teamSliderEl, teamThumbsEl);
		SWIPER.slider('.why-swiper-container');
	}
};

export default {
	init : SWIPER.init
};
