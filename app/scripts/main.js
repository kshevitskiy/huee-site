//** Components
import './data_vue'
import './huee'
import './medium'

import swiper from './swiper'
import forms from './forms'
// import animation from './animations'
// import ui from './ui'

swiper.init();
forms.init();
// animation.init();
// ui.init();

function toggleInfoBox() {
    if ($("#Section-Intro").hasClass('active')) {
        setTimeout(function(){
            $('.toggleInfoBox').addClass('el-fadeOut');
        }, 6000);
    } else {
        $('.toggleInfoBox').removeClass('el-fadeOut');
    }
};

setInterval(function(){
    toggleInfoBox()
}, 1000);