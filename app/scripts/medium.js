import moment from 'moment/src/moment';
import { Swiper, Navigation, Controller } from 'swiper/dist/js/swiper.esm.js';
Swiper.use([Navigation, Controller]);

(function e(t, n, r) {
    function s(o, u) {
        if (!n[o]) {
            if (!t[o]) {
                var a = typeof require == "function" && require;if (!u && a) return a(o, !0);if (i) return i(o, !0);throw new Error("Cannot find module '" + o + "'");
            }var f = n[o] = { exports: {} };t[o][0].call(f.exports, function (e) {
                var n = t[o][1][e];return s(n ? n : e);
            }, f, f.exports, e, t, n, r);
        }return n[o].exports;
    }var i = typeof require == "function" && require;for (var o = 0; o < r.length; o++) {
        s(r[o]);
    }return s;
})({ 1: [function (require, module, exports) {
        //** Components

        var ui = require('./ui');
        var medium = require('./medium');

        window.onload = function () {
            ui.init();
            medium.init();
        };
    }, { "./medium": 2, "./ui": 3 }], 2: [function (require, module, exports) {
        //** Import utilities
        var util = require('./utilities');

        var mediumWrapper = document.getElementById('medium');

        //** Slider markup
        var swiperWrapper = document.createElement('div');
        swiperWrapper.setAttribute('class', 'swiper-wrapper');
        var nextSlide = document.createElement('button');
        nextSlide.setAttribute('class', 'medium-control next-medium-slide');
        nextSlide.innerHTML = '<span class="arrow arrow-right"></span>';
        var prevSlide = document.createElement('button');
        prevSlide.setAttribute('class', 'medium-control prev-medium-slide');
        prevSlide.innerHTML = '<span class="arrow arrow-left"></span>';
        var pagination = document.createElement('div');
        pagination.setAttribute('class', 'medium-pagination swiper-pagination');

        var url = 'https://api.rss2json.com/v1/api.json?rss_url=https://medium.com/feed/@huee';
        var slideItem = '';
        var postRender = '';

        var MEDIUM = {

            getImageUrl: function getImageUrl(source) {
                var tagIndex = source.indexOf('<img'),
                    srcIndex = source.substring(tagIndex).indexOf('src=') + tagIndex,
                    srcStart = srcIndex + 5,
                    srcEnd = source.substring(srcStart).indexOf('"') + srcStart,
                    url = source.substring(srcStart, srcEnd);
                return url;
            },

            posts: function posts() {

                var xhr = new XMLHttpRequest();
                xhr.onload = function () {
                    if (xhr.readyState === xhr.DONE) {
                        if (xhr.status === 200) {
                            var posts = xhr.response.items;

                            var slides = void 0;

                            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                                slides = util.chunkArray(posts, 3);
                            } else {
                                slides = util.chunkArray(posts, 6);
                            }

                            // Slide
                            slides.some(function (slide, i) {

                                // Render slide
                                slideItem = document.createElement('div');
                                slideItem.setAttribute('id', 'slide-' + i + '');
                                slideItem.setAttribute('class', 'slide swiper-slide');

                                for (var post = 0; post < slide.length; post++) {
                                    var date = util.dateFormat(slide[post].pubDate, 'DD.MM.YYYY'),
                                        author = slide[post].author,
                                        link = slide[post].link,
                                        title = slide[post].title,
                                        description = slide[post].description,
                                        imgUrl = MEDIUM.getImageUrl(description),
                                        copy = util.textFormat(slide[post].description);
                                    // shortTitle = util.trimText(title, 40);


                                    var postMarkup = '<a href="' + link + '" target="_blank" title="' + title + '" class="NewsItem NewsItem_Text OutboundLink">' + '<div>' + '<div class="Date">' + date + ' by huee&trade; ' + '</div>' + '<h4>' + title + '</h4>' + '</div>' + '</a>';

                                    // Render post
                                    postRender = document.createElement('div');
                                    postRender.setAttribute('class', 'post fade-in-up');
                                    postRender.innerHTML = postMarkup;

                                    slideItem.appendChild(postRender);
                                };

                                mediumWrapper.appendChild(swiperWrapper);
                                swiperWrapper.appendChild(slideItem);

                                return i === 2;
                            });

                            MEDIUM.slider(mediumWrapper);
                            mediumWrapper.appendChild(prevSlide);
                            mediumWrapper.appendChild(nextSlide);
                            mediumWrapper.appendChild(pagination);
                        } else {
                            MEDIUM.error();
                        }
                    }
                };
                xhr.open('GET', url, true);
                xhr.responseType = 'json';
                xhr.send(null);
            },

            error: function error() {
                console.log('Error, please reaload page');
            },

            slider: function slider(element) {
                var swiper = new Swiper(element, {
                    navigation: {
                        nextEl: nextSlide,
                        prevEl: prevSlide
                    },
                    pagination: {
                        el: pagination,
                        clickable: true
                    },
                    spaceBetween: 50,
                    speed: 1000,
                    on: {
                        init: function init() {
                            nextSlide.classList.add('active');
                        },

                        reachBeginning: function reachBeginning() {
                            nextSlide.classList.add('active');
                            prevSlide.classList.remove('active');
                        },

                        reachEnd: function reachEnd() {
                            prevSlide.classList.add('active');
                            nextSlide.classList.remove('active');
                        }
                    }
                });
            },

            init: function init() {
                MEDIUM.posts();
            }
        };

        module.exports = {
            init: MEDIUM.init
        };
    }, { "./utilities": 4 }], 3: [function (require, module, exports) {
        var UI = {

            init: function init() {
                // console.log('Hello');
            }
        };

        module.exports = {
            init: UI.init
        };
    }, {}], 4: [function (require, module, exports) {
        var UTILITIES = {

            dateFormat: function dateFormat(date, format) {
                var m = moment(date).format(format);
                return m;
            },

            textFormat: function textFormat(text) {
                var content = text.replace(/<img[^>]*>/g, ""),
                    maxLength = 200,
                    trimmedContent = content.substr(0, maxLength);

                trimmedContent = trimmedContent.substr(0, Math.min(trimmedContent.length, trimmedContent.lastIndexOf(' ')));
                return trimmedContent;
            },

            chunkArray: function chunkArray(myArray, chunkSize) {
                /**
                 * Returns an array with arrays of the given size.
                 *
                 * @param myArray {Array} array to split
                 * @param chunkSize {Integer} Size of every group
                 */
                var index = 0;
                var arrayLength = myArray.length;
                var tempArray = [];

                for (index = 0; index < arrayLength; index += chunkSize) {
                    var myChunk = myArray.slice(index, index + chunkSize);
                    // Do something if you want with the group            
                    tempArray.push(myChunk);
                }
                return tempArray;
            },

            trimText: function trimText(str, length, ending) {

                if (length == null) {
                    length = 100;
                }

                if (ending == null) {
                    ending = '...';
                }

                if (str.length > length) {
                    var trimmedString = str.substr(0, length);
                    trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(' ')));
                    return trimmedString + ending;
                } else {
                    return str;
                }
            }
        };

        module.exports = {
            dateFormat: UTILITIES.dateFormat,
            textFormat: UTILITIES.textFormat,
            chunkArray: UTILITIES.chunkArray,
            trimText: UTILITIES.trimText
        };
    }, {}] }, {}, [1]);        