const projects = document.querySelectorAll('.SectionStack_Portfolio_Video');


projects.forEach(function(project, index) {
    project.addEventListener('mouseenter', startVideo, false);
    project.addEventListener('mouseleave', stopVideo, false);
});

    // function startVideo(e) {
    //     e.target.play();
    // } function() {
    //     e.target.pause();
    // }

    function startVideo(e) {
        e.target.play();
    }

    function stopVideo(e) {
        e.target.pause();
    }

jQuery(function($) {

    (function(scope) {
        var dragging = false;
        var lastY = 0;

        function dragStart(event) {
        dragging = true;
        this.style.pointerEvents = 'none';
        this.style.userSelect = 'none';

        lastY = (event.clientY || event.clientY === 0) ? event.clientY : event.touches[0].clientY;
        }

        function dragMove(event) {
        if (!dragging) return;
        var clientY = (event.clientY || event.clientY === 0) ? event.clientY : event.touches[0].clientY;
        this.scrollTop += (clientY - lastY)/this.thumb.scaling;
        lastY = clientY;
        event.preventDefault();
        }

        function dragEnd(event) {
        dragging = false;
        this.style.pointerEvents = 'initial';
        this.style.userSelect = 'initial';
        }


        // The point of this function is to update the thumb's geometry to reflect
        // the amount of overflow.
        function updateSize(scrollable) {
        scrollable.style.width = '';
        scrollable.style.width = `calc(${getComputedStyle(scrollable).width} + 50px)`;

        var thumb = scrollable.thumb;
        var viewport = scrollable.getBoundingClientRect();
        var scrollHeight = scrollable.scrollHeight;
        var maxScrollTop = scrollHeight - viewport.height;
        var thumbHeight = Math.pow(viewport.height, 2)/scrollHeight;
        var maxTopOffset = viewport.height - thumbHeight;

        thumb.scaling = maxTopOffset / maxScrollTop;
        thumb.style.height = `${thumbHeight}px`;

        if(scrollable.isIOS) {
            thumb.nextElementSibling.style.marginTop = `-${thumbHeight}px`;
            var z = 1 - 1/(1+thumb.scaling);
            thumb.style.transform = `
            translateZ(${z}px)
            scale(${1-z})
            translateX(-50px)
            `;
        } else {
            thumb.style.transform = `
            scale(${1/thumb.scaling})
            matrix3d(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, -1
            )
            translateZ(${-2 + 1 - 1/thumb.scaling}px)
            translateX(-50px)
            `;
        }
        }

        function makeCustomScrollbar(scrollable) {
        if (getComputedStyle(document.body).transform == 'none')
            document.body.style.transform = 'translateZ(0)';
        var fixedPos = document.createElement('div');
        fixedPos.style.position = 'fixed';
        fixedPos.style.top = '0';
        fixedPos.style.width = '1px';
        fixedPos.style.height = '1px';
        fixedPos.style.zIndex = 1;
        document.body.insertBefore(fixedPos, document.body.firstChild);

        scrollable.style.perspectiveOrigin = 'top right';
        scrollable.style.transformStyle = 'preserve-3d';
        scrollable.style.perspective = '1px';

        var perspectiveCtr = document.createElement('div');
        perspectiveCtr.style.perspectiveOrigin = 'top right';
        perspectiveCtr.style.transformStyle = 'preserve-3d';
        perspectiveCtr.style.width = '100%';

        perspectiveCtr.style.position = 'absolute';
        perspectiveCtr.style.pointerEvents = 'none';
        perspectiveCtr.classList.add('perspective-ctr')

        while(scrollable.firstChild) perspectiveCtr.appendChild(scrollable.firstChild);

        scrollable.insertBefore(perspectiveCtr, scrollable.firstChild);
        var thumb = document.createElement('div');
        thumb.classList.add('thumb');
        thumb.style.pointerEvents = 'initial';
        thumb.style.position = 'absolute';
        thumb.style.transformOrigin = 'top right';
        thumb.style.top = '0';
        thumb.style.right = '0';
        perspectiveCtr.insertBefore(thumb, perspectiveCtr.firstChild);
        scrollable.thumb = thumb;
        scrollable.perspectiveCtr = perspectiveCtr;

        // We are on Safari, where we need to use the sticky trick!
        if (getComputedStyle(scrollable).webkitOverflowScrolling) {
            scrollable.isIOS = true;
            thumb.style.right = '';
            thumb.style.left = '100%';
            thumb.style.position = '-webkit-sticky';
            perspectiveCtr.style.perspective = '1px';
            perspectiveCtr.style.height = '';
            perspectiveCtr.style.width = '';
            perspectiveCtr.style.position = '';
            Array.from(scrollable.children)
            .filter(function (e) {return e !== perspectiveCtr;})
            .forEach(function (e) {perspectiveCtr.appendChild(e);});
        }

        scrollable.thumb.addEventListener('mousedown', dragStart.bind(scrollable), {passive: true});
        window.addEventListener('mousemove', dragMove.bind(scrollable));
        window.addEventListener('mouseup', dragEnd.bind(scrollable), {passive: true});
        scrollable.thumb.addEventListener('touchstart', dragStart.bind(scrollable), {passive: true});
        window.addEventListener('touchmove', dragMove.bind(scrollable));
        window.addEventListener('touchend', dragEnd.bind(scrollable), {passive: true});

        var f = function () {
            updateSize(scrollable);
        };
        requestAnimationFrame(f);
        window.addEventListener('resize', f);
        return f;
        }

        window.addEventListener('load', function () {
        Array.from(document.querySelectorAll('.overflow')).forEach(scrollable => {
            makeCustomScrollbar(scrollable);
            updateSize(scrollable);
        });
        });

    })(self);


    setTimeout(function() {
        $("#Preloader .SvgWrap").animate({
            opacity: 0
        }, 500, function() {
            $("#Preloader").animate({
                opacity: 0
            }, 500, function() {
                if ($('#HueeFullpage').length) {
                    $.fn.fullpage.setAllowScrolling(true);
                }
                clearInterval(ProgressInterval);
                $("#Preloader").remove();
                $("body").css("overflow", "visible");
            });
        });
        if ($("#Section-Intro").hasClass("active")) {
            $(IntroAudio).animate({
                volume: 0.4
            }, 700);
        }
    }, 1500);
    var AnimPerformance = true;
    var Sections = [];
    var Animations = [];
    var IntroAudio;
    $('#HueeFullpage .section').each(function() {
        Sections.push($(this).attr("data-id"));
    });
    var PortfolioSlides = $("#Section-Projects > .slide").length;
    PortfolioSlides--;
    $("head").append("<style type='text/css'>body.fp-viewing-workhighlights-" + PortfolioSlides + " .Section-Projects .fp-controlArrow.fp-next::after { transform:translateX(5px) rotate(225deg); -webkit-transform:translateX(5px) rotate(225deg); }	body.fp-viewing-workhighlights-" + PortfolioSlides + " .Section-Projects .fp-controlArrow.fp-next:hover::after { transform: translateX(-5px) rotate(115deg); -webkit-transform: translateX(-5px) rotate(225deg); }</style>");

    function LoadPopup() {
        $(window).load(function() {
            setTimeout(function() {
                if ($("body").width() > 640 && $("#Section-Intro").hasClass("active")) {
                    $("#SectionStack_Intro_Actions_Hit").trigger("click");
                }
            }, 5000);
        });
    }
    
    $('#HueeFullpage').fullpage({
        anchors: Sections,
        scrollingSpeed: 1000,
        navigation: true,
        loopHorizontal: true,
        navigationPosition: 'left',
        dragAndMove: true,
        dragAndMoveKey: 'aHVlZS5pbl9JMjhaSEpoWjBGdVpFMXZkbVU9ZjZ2',
        resetSliders: false,
        keyboardScrolling: false,
        slidesNavigation: true,
        scrollOverflow: true,
        scrollOverflowOptions: {
            probeType: 2
        },
        scrollBar: false,
        onLeave: function(index, nextIndex, direction) {
            if ($('#HueeFullpage .section:nth-child(' + nextIndex + ') .slide.active').length) {
                $("body").attr("data-color", $('#HueeFullpage .section:nth-child(' + nextIndex + ') .slide.active').attr("data-color"));
                $("body").attr("data-mobilecolor", $('#HueeFullpage .section:nth-child(' + nextIndex + ') .slide.active').attr("data-mobilecolor"));
                $("#HueeCurrent").html($('#HueeFullpage .section:nth-child(' + nextIndex + ') .slide.active').attr("data-name"));
            } else {
                $("body").attr("data-color", $('#HueeFullpage .section:nth-child(' + nextIndex + ')').attr("data-color"));
                $("body").attr("data-mobilecolor", $('#HueeFullpage .section:nth-child(' + nextIndex + ')').attr("data-mobilecolor"));
                $("#HueeCurrent").html($('#HueeFullpage .section:nth-child(' + nextIndex + ')').attr("data-name"));
            }
            if (nextIndex == "1") {
                IntroAudio.currentTime = 0;
                $(IntroAudio).animate({
                    volume: 0.4
                }, 300);
            } else {
                $(IntroAudio).animate({
                    volume: 0
                }, 300, function() {
                    IntroAudio.pause();
                });
            }
        },
        onSlideLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex) {
            nextSlideIndex = nextSlideIndex + 1;
            $("body").attr("data-color", $('#HueeFullpage .section:nth-child(' + index + ') .slide:nth-child(' + nextSlideIndex + ')').attr("data-color"));
            $("body").attr("data-mobilecolor", $('#HueeFullpage .section:nth-child(' + index + ') .slide:nth-child(' + nextSlideIndex + ')').attr("data-mobilecolor"));
            $("#HueeCurrent").html($('#HueeFullpage .section:nth-child(' + index + ') .slide:nth-child(' + nextSlideIndex + ')').attr("data-name"));
        },
        afterLoad: function(anchorLink, index) {
            if ($('#HueeFullpage .section:nth-child(' + index + ') .slide.active').length) {
                $("body").attr("data-color", $('#HueeFullpage .section:nth-child(' + index + ') .slide.active').attr("data-color"));
                $("body").attr("data-mobilecolor", $('#HueeFullpage .section:nth-child(' + index + ') .slide.active').attr("data-mobilecolor"));
                $("#HueeCurrent").html($('#HueeFullpage .section:nth-child(' + index + ') .slide.activ').attr("data-name"));
            } else {
                $("body").attr("data-color", $('#HueeFullpage .section:nth-child(' + index + ')').attr("data-color"));
                $("body").attr("data-mobilecolor", $('#HueeFullpage .section:nth-child(' + index + ')').attr("data-mobilecolor"));
                $("#HueeCurrent").html($('#HueeFullpage .section:nth-child(' + index + ')').attr("data-name"));
            }
        }
    });
    if ($('#HueeFullpage').length) {
        $.fn.fullpage.setAllowScrolling(false);
    }
    $("body").on("click", "#HueeCurrent a[data-href]", function() {
        $.fn.fullpage.moveTo($(this).attr("data-href"), $(this).attr("data-slide"));
    });
    $(".SlideRight").click(function() {
        $.fn.fullpage.moveSlideRight();
        return false;
    })
    $(".SlideLeft").click(function() {
        $.fn.fullpage.moveSlideLeft();
        return false;
    })
    $(".SectionUp").click(function() {
        $.fn.fullpage.moveUp();
        return false;
    })
    $(".SectionDown").click(function() {
        $.fn.fullpage.moveDown();
        return false;
    })
    var ProgressStatus = 0;
    var PreloderTaglineAnimated = false;
    var ProgressInterval = setInterval(function() {
        ProgressStatus += (100 - ProgressStatus) / 5;
        $("#PreloaderProgress").css("width", ProgressStatus + "%");
        if (ProgressStatus > 45 && !PreloderTaglineAnimated) {
            PreloderTaglineAnimated = true;
            $("#PreloaderTagline").animate({
                opacity: 1
            }, 800);
        }
    }, 600);
    $("#Preloader .SvgWrap").animate({
        opacity: 1
    }, 300);


    var ua = window.navigator.userAgent.toLowerCase();

    var fn = {
        $: function(id) {
        return document.querySelector(id);
        },
        on: function(type, element, handler, userCapture) {
        if (element.addEventListener) {
            element.addEventListener(type, handler, userCapture);
        } else if (element.attachEvent) {
            element.attachEvent("on" + type, handler);
        }
        },
        off: function(type, element, handler) {
        if (element.removeEventListener) {
            element.removeEventListener(type, handler);
        } else if (element.detachEvent) {
            element.detachEvent("on" + type, handler);
        }
        },
        browser: {
        ie: function() {
            var s = ua.match(/rv:([\d.]+)\) like gecko/) || ua.match(/msie ([\d.]+)/);
            return s && s[1];
        }(),
        chrome: function() {
            var s = ua.match(/chrome\/([\d.]+)/);
            return s && s[1];
        }(),
        safari: function() {
            var s = ua.match(/version\/([\d.]+).*safari/);
            return s && s[1];
        }(),
        firefox: function() {
            var s = ua.match(/firefox\/([\d.]+)/);
            return s && s[1];
        }()
        }
    };

    function wheelbind(element, handler, userCapture) {
        if (fn.browser.chrome || fn.browser.safari || fn.browser.ie) { //Except FireFox
        fn.on("mousewheel", element, function(ev) {
            var event = ev || window.event;
            var delta = event.wheelDelta; //向上滚动120，向下滚动-120
            event.preventDefault ? event.preventDefault() : event.returnValue = false;
            handler(delta);
        }, userCapture);
        } else {
        fn.on("DOMMouseScroll", element, function(ev) { //FireFox
            var event = ev || window.event;
            var delta = event.detail > 0 ? -1 : 1; //detail值不定，向上滚动+1，向下滚动-1
            event.preventDefault();
            handler(delta);
        }, userCapture);
        }
    }

    function SectionStack_VideoOverlay() {
        $(".SectionStack_VideoOverlay").fadeOut(0);
        $(".SectionStack_VideoOverlay_Close").click(function() {
            $(this).parent().parent().fadeOut();
            return false;
        });
    }

    function YTKeepRatio() {
        function ResizeHandler() {
            $(".YTKeepRatio").each(function() {
                $(this).height($(this).width() / 16 * 9);
            });
        }
        ResizeHandler();
        $(window).resize(ResizeHandler);
    }

    function Burger() {
        var BurgerAnimated = false;
        $('#HueeBurger').click(function() {
            if (BurgerAnimated) {
                return false;
            }
            BurgerAnimated = true;
            if ($("#Overlay-Menu").hasClass("Active")) {
                $(this).removeClass('Active');
                $("#Overlay-Menu").removeClass('Active');
                setTimeout(function() {
                    $("#OverlayBgAnim-Red").removeClass('Active');
                    $("body").removeClass('OverlayOn');
                    BurgerAnimated = false;
                }, 300);
            } else {
                $(this).addClass('Active');
                $("#OverlayBgAnim-Red").css("background", $("#Menu_MainNav").attr('data-bg'));
                $("#OverlayBgAnim-Red").addClass('Active');
                $("body").addClass('OverlayOn');
                setTimeout(function() {
                    $("#Overlay-Menu").addClass('Active');
                    BurgerAnimated = false;
                }, 1000);
            }
        });
        $("body.Home #Overlay-Menu_MainNav a").click(function() {
            if (BurgerAnimated) {
                return false;
            }
            BurgerAnimated = true;
            $("#HueeBurger").removeClass('Active');
            $("#Overlay-Menu").removeClass('Active');
            setTimeout(function() {
                $("#OverlayBgAnim-Red").removeClass('Active');
                $("body").removeClass('OverlayOn');
                $("#OverlayBgAnim-Red").css("background", $("#Menu_MainNav").attr('data-bg'));
                BurgerAnimated = false;
            }, 300);
        });
        $("#Overlay-Menu a[data-bg]").mouseout(function() {
            $("#OverlayBgAnim-Red").css("background", $("#Overlay-Menu_MainNav").attr('data-bg'));
        });
        $("#Overlay-Menu a[data-bg]").mouseover(function() {
            $("#OverlayBgAnim-Red").css("background", $(this).attr('data-bg'));
        });
    }

    function AtentionSection() {
        $("#Section-Attention_AttentionTxt > div").mouseover(function() {
            $("#Section-Attention_Darkener").addClass("Active");
        });
        $("#Section-Attention_AttentionTxt > div").mouseleave(function() {
            $("#Section-Attention_Darkener").removeClass("Active");
        });
    }

    function Mission() {
        if (!$("#Slide-Mission").length) return false;
        CharSelect();

        function FaciSelect() {
            var FaciOwl = $("#Facilities").owlCarousel({
                items: 1,
                margin: 0,
                autoplay: false,
                loop: true,
                nav: true,
                navText: ['', '']
            });
            FaciOwl.on('translated.owl.carousel', function(event) {
                $("#FacilityParams .CharacterName").html($("#Facilities .owl-item:nth-child(" + (event.item.index + 1) + ") img").attr("data-name"));
                $("#FacilityParams .CharacterPower").html($("#Facilities .owl-item:nth-child(" + (event.item.index + 1) + ") img").attr("data-power"));
                $("#FacilityParams .CharacterPowerDesc").html($("#Facilities .owl-item:nth-child(" + (event.item.index + 1) + ") img").attr("data-powerdesc"));
                $("#Vision_Vehicle").attr("data-facility", $("#Facilities .owl-item:nth-child(" + (event.item.index + 1) + ") img").attr("data-facility"));
                $("#Change_Vehicle").attr("data-facility", $("#Facilities .owl-item:nth-child(" + (event.item.index + 1) + ") img").attr("data-facility"));
            });
            $("#MissionSelectGoMissionBtn").click(function() {
                $("#FacilitySelect").animate({
                    opacity: 0
                }, 500, function() {
                    $("#FacilitySelect").css("display", "none");
                    $("#CharacterSelect").css("display", "block");
                    $("#CharacterSelect").animate({
                        opacity: 1
                    }, 500);
                });
                return false;
            });
        }

        function CharSelect() {
            var CharOwl = $("#Characters").owlCarousel({
                items: 1,
                margin: 0,
                autoplay: false,
                loop: true,
                nav: true,
                navText: ['', '']
            });
            CharOwl.on('translated.owl.carousel', function(event) {
                $("#CharacterParams .CharacterName").html($("#Characters .owl-item:nth-child(" + (event.item.index + 1) + ") img").attr("data-name"));
                $("#CharacterParams .CharacterPower").html($("#Characters .owl-item:nth-child(" + (event.item.index + 1) + ") img").attr("data-power"));
                $("#CharacterParams .CharacterPowerDesc").html($("#Characters .owl-item:nth-child(" + (event.item.index + 1) + ") img").attr("data-powerdesc"));
                $("#Vision_Vehicle").attr("data-hero", $("#Characters .owl-item:nth-child(" + (event.item.index + 1) + ") img").attr("data-hero"));
                $("#Change_Vehicle").attr("data-hero", $("#Characters .owl-item:nth-child(" + (event.item.index + 1) + ") img").attr("data-hero"));
            })
            $("#MissionSelectFacilityBtn").click(function() {
                $("#CharacterSelect").animate({
                    opacity: 0
                }, 500, function() {
                    $("#CharacterSelect").css("display", "none");
                    $("#FacilitySelect").css("display", "block");
                    FaciSelect();
                    $("#FacilitySelect").animate({
                        opacity: 1
                    }, 500);
                });
                return false;
            });
        }

        function Polygon() {
            var Polys = [5, 48, 95, 15, 55, 95];
            var Canv = Snap('#CharactersPolygon');
            var Polygon = Canv.polygon().attr({
                points: Polys,
                fill: '#ffffff'
            });

            function Animation() {
                var NewPoly = [];
                $(Polys).each(function() {
                    NewPoly.push(this + Math.floor(Math.random() * 10 - 5));
                });
            }
            Animation();
        }
    }

    function AboutSection() {
        if (!$("#Section-About").length) return false;
    }

    function CapabilitiesSection() {
        if (!$("#Section-Capabilities").length) return false;
        Mobile();
        SideArt();

        function Mobile() {
            $(".SectionStack_CapabilitiesStatic_ShowWeTouch").click(function() {
                $(this).parent().removeClass("Active");
                $(this).parent().next().addClass("Active");
                return false;
            });
            $(".SectionStack_CapabilitiesStatic_ShowBack").click(function() {
                $(this).parent().removeClass("Active");
                $(this).parent().prev().addClass("Active");
                return false;
            });
        }

        function SideArt() {
            var CapabilitiesSideArtAnim = false;
            $("#CapabilitiesWeTouch a").click(function() {
                $.fn.fullpage.setAllowScrolling(false);
                if (CapabilitiesSideArtAnim) return;
                CapabilitiesSideArtAnim = true;
                var Index = $(this).parent().index() + 1;
                if ($("#CapabilitiesSideArt").hasClass("Active")) {
                    $("#CapabilitiesSideArt > .Active").animate({
                        opacity: 0
                    }, 300, function() {
                        $("#CapabilitiesSideArt > .Active").removeClass("Active");
                        $("#CapabilitiesSideArt > div:nth-child(" + Index + ")").addClass("Active");
                        $("#CapabilitiesSideArt > div:nth-child(" + Index + ")").animate({
                            opacity: 1
                        }, 300, function() {
                            CapabilitiesSideArtAnim = false
                        });
                    });
                } else {
                    $("#CapabilitiesSideArt").addClass("Active");
                    $("#HueeHeader").addClass("ForceBlackLogo");
                    setTimeout(function() {
                        $("#CapabilitiesSideArt > div:nth-child(" + Index + ")").addClass("Active");
                        $("#CapabilitiesSideArt > div:nth-child(" + Index + ")").animate({
                            opacity: 1
                        }, 300, function() {
                            CapabilitiesSideArtAnim = false
                        });
                    }, 500);
                }
                return false;
            });
            $("#CapabilitiesSideArtClose, .SectionStack_SideArt_CloseBtn").click(function() {
                if (CapabilitiesSideArtAnim) return;
                CapabilitiesSideArtAnim = true;
                $.fn.fullpage.setAllowScrolling(true);
                $("#CapabilitiesSideArt > .Active").animate({
                    opacity: 0
                }, 300, function() {
                    $("#HueeHeader").removeClass("ForceBlackLogo");
                    $("#CapabilitiesSideArt > .Active").removeClass("Active");
                    $("#CapabilitiesSideArt").removeClass("Active");
                });
                setTimeout(function() {
                    CapabilitiesSideArtAnim = false
                }, 600);
                return false;
            });
        }
    }

	function ChangeSection() {
		if (!$("#Slide-Change").length) return false;
			
		var List = $("#ChangeContent").attr("data-content");
		if (List) { 
			List = JSON.parse(List); 
				
			var CurrentTxt = 0;
			var AllTxt = List.length - 1;
						
			$("#ChangeMainTitle").html(List[0][0]);
			$("#ChangeContent").html(List[0][1]);
			
			var	FillBtn;
			var FillBtnProgress = 0;
			var FillBtnState = false;
			var FillBtnFull = false;
			$(document).on('keydown', function(e) { if(e.keyCode == 32) { ChangeKeyDown(); }}).on('keyup', function(e) { if(e.keyCode == 32) { ChangeKeyUp(); }})
			$("#ChangeSlide_FillChange").on('mousedown touchstart', ChangeKeyDown).on('mouseup touchend', ChangeKeyUp)
			
			function ChangeKeyDown(e) {
				if(FillBtnFull) { return false; }
				if(!$("#Slide-Change").hasClass("active")) { return false; }				
				if(FillBtnState == "DOWN") { return false; }
				clearInterval(FillBtn);
				FillBtnState = "DOWN";
				FillBtn = setInterval(function() {
					if (FillBtnProgress > 20) {
						$("#ChangeSlide_FillChange").addClass("Filled");
					}
					if (FillBtnProgress < 100) {
						FillBtnProgress += 1;
						$("#ChangeSlide_FillChange > span:first-child").css("transform","translateX("+FillBtnProgress+"%)").css("-webkit-transform","translateX("+FillBtnProgress+"%)");
					} else {
						FillBtnState = false;
						FillBtnFull = true;
						clearInterval(FillBtn);
						Change();
					}
				}, 7);
			}
			function ChangeKeyUp(e) {
				if(FillBtnFull) { return false; }
				if(!$("#Slide-Change").hasClass("active")) { return false; }
				if(FillBtnState == "UP") { return false; }
				clearInterval(FillBtn);
				FillBtnState = "UP";
				FillBtn = setInterval(function() {
					if (FillBtnProgress < 80) {
						$("#ChangeSlide_FillChange").removeClass("Filled");
					}
					if (FillBtnProgress > 0) {
						FillBtnProgress -= 4;
						$("#ChangeSlide_FillChange > span:first-child").css("transform","translateX("+FillBtnProgress+"%)").css("-webkit-transform","translateX("+FillBtnProgress+"%)");
					} else {
						FillBtnState = false;
						clearInterval(FillBtn);
					}
				}, 7);	
			}		
			
			function randomRange(min, max) {
				return Math.floor(Math.random() * (max - min + 1)) + min;
			}
			
			function Change() { 
				CurrentTxt++;
				if (CurrentTxt > AllTxt) { CurrentTxt=0; }
				 
				$("#ChangeContent").animate({opacity: 0}, 300, function() {
					$("#ChangeMainTitle").html(List[CurrentTxt][0]);
					$("#ChangeContent").html(List[CurrentTxt][1]);
					$("#ChangeContent").animate({opacity: 1}, 300);
					$("#ChangeSlide_FillChange").removeClass("Filled");
					$("#ChangeSlide_FillChange > span:first-child").animate({opacity:0}, 800, function() {
						$("#ChangeSlide_FillChange > span:first-child").css("opacity","1").css("transform","translateX(0%)").css("-webkit-transform","translateX(0%)");
						FillBtnProgress = 0;
						FillBtnFull = false;
					});
				});
				
				$("#ChangeGun").addClass("Active");
				setTimeout(function() { 
					$("#ChangeGun").addClass("Shoot"); 
					if ($("#ChangeItems img.Active").index() < $("#ChangeItems img").length - 1) {
						$("#ChangeItems img.Active").next().addClass("Active"); 
						$("#ChangeItems img.Active").eq(0).removeClass("Active"); 
					} else {
						$("#ChangeItems img.Active").removeClass("Active"); 
						$("#ChangeItems img:first-child").addClass("Active"); 						
					}
				}, 800);
				setTimeout(function() { $("#ChangeGun").removeClass("Shoot"); }, 1000);
				setTimeout(function() { $("#ChangeGun").removeClass("Active"); }, 1200);
				
				return false;
			}
			
		} 
	}    

    function Share() {
        $(".ShareLink").click(function(e) {
            e.preventDefault();
            window.open($(this).attr("href"), 'Share', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=650,height=450,top=50,left=50');
            return false;
        });
    }

    /////////////////////////// SECTIONS

    function IntroSection() {
        if (!$("#Section-Intro").length) return false;
        var ValidateTimeout;
        IntroNavs();
        TellAFriend();
        NumberEnter();
        Sound();

        function Sound() {
            IntroAudio = new Audio();
            IntroAudio.src = "_video/ambient.mp3";
            IntroAudio.volume = 0;
            IntroAudio.addEventListener('ended', function() {
                this.currentTime = 0;
                this.play();
            }, false);
        }

        function IntroNavs() {
            // $("#SectionStack_Intro_Actions_Vr").click(function() {
            //     $("#SectionStack_VideoOverlay_Vr").fadeIn(300);
            //     $("#SectionStack_VideoOverlay_Vr .YTKeepRatio").each(function() {
            //         $(this).height($(this).width() / 16 * 9);
            //     });
            //     return false;
            // });
            $("#SectionStack_Intro_Actions_Hit").click(function() {
                $("#SectionStack_VideoOverlay_Hit").fadeIn(300);
                $("#SectionStack_VideoOverlay_Hit .YTKeepRatio").each(function() {
                    $(this).height($(this).width() / 16 * 9);
                });
                return false;
            });
            $(".SectionStack_Intro_ActionsContainer_Back").click(function() {
                $("#SectionStack_Intro_ActionsContainer > div").fadeOut(300);
                setTimeout(function() {
                    $("#SectionStack_Intro_Fader").fadeOut(300);
                    $("#SectionStack_Intro_ActionsContainer_CallInput").fadeIn(300);
                    $("#SectionStack_Intro_ActionsContainer_Taf_Steps > div:nth-child(1)").fadeIn(300);
                    $("#SectionStack_Intro_ActionsContainer_Taf_Steps > div:not(:nth-child(1))").fadeOut(300);
                }, 400);
                return false;
            });
            $("#SectionStack_Intro_ActionsContainer_Taf").fadeOut(0);
            $("#SectionStack_Intro_ActionsContainer_Share").fadeOut(0);
            $("#SectionStack_Intro_Fader").fadeOut(0);
            $("#SectionStack_Intro_ActionsContainer_Taf_Steps > div:not(:nth-child(1))").fadeOut(0);
            $("#SectionStack_Intro_Actions_Share").click(function() {
                $("#SectionStack_Intro_ActionsContainer > div").fadeOut(300);
                setTimeout(function() {
                    $("#SectionStack_Intro_Fader").fadeIn(300);
                    $("#SectionStack_Intro_ActionsContainer_Share").fadeIn(300);
                }, 400);
                return false;
            });
            $("#SectionStack_Intro_Actions_Taf").click(function() {
                $("#SectionStack_Intro_ActionsContainer > div").fadeOut(300);
                setTimeout(function() {
                    $("#SectionStack_Intro_Fader").fadeIn(300);
                    $("#SectionStack_Intro_ActionsContainer_Taf").fadeIn(300);
                }, 400);
                return false;
            });
        }

        function TellAFriend() {
            $("#SectionStack_Intro_ActionsContainer_Taf_Next").click(function() {
                $("#SectionStack_Intro_ActionsContainer_Taf_Steps > div:nth-child(1)").fadeOut(300, function() {
                    $("#SectionStack_Intro_ActionsContainer_Taf_Steps > div:nth-child(2)").fadeIn(300);
                });
                return false;
            });
            $("#SectionStack_Intro_ActionsContainer_Taf_NextB").click(function() {
                $("#SectionStack_Intro_ActionsContainer_Taf_Steps > div:nth-child(2)").fadeOut(300, function() {
                    $("#SectionStack_Intro_ActionsContainer_Taf_Steps > div:nth-child(3)").fadeIn(300);
                });
                return false;
            });
            $("#SectionStack_Intro_ActionsContainer_Taf_Send").click(function() {
                $("#SectionStack_Intro_ActionsContainer_Taf_Steps > div:nth-child(3)").fadeOut(300);
                var BuddyName = $("#IntroTafBName").val();
                var BuddyMail = $("#IntroTafBMail").val();
                var Name = $("#IntroTafName").val();
                $.ajax({
                    type: 'POST',
                    url: '/_twilio/taf.php',
                    data: {
                        "BuddyName": BuddyName,
                        "BuddyMail": BuddyMail,
                        "Name": Name
                    }
                }).done(function(response) {
                    if (response == "1") {
                        $("#IntroTafStatusSuccess").css("display", "block");
                    } else {
                        $("#IntroTafStatusFail").css("display", "block");
                    }
                    $("#SectionStack_Intro_ActionsContainer_Taf_Steps > div:nth-child(4)").fadeIn(300);
                }).fail(function(data) {
                    $("#IntroTafStatusFail").css("display", "block");
                    $("#SectionStack_Intro_ActionsContainer_Taf_Steps > div:nth-child(4)").fadeIn(300);
                });
                return false;
            });
        }

        function NumberEnter() {
            var audio = [];

            for (var i = 0; i < 10; i++) {
                audio[i] = new Audio();
                audio[i].src = '/_sounds/' + i + '.mp3';
                audio[i].volume = 0.5;
            }

            $("#SectionStack_Intro_ActionsContainer_CallInput_Number").on("keydown", function(e) {
                if (e.keyCode == 48 || e.keyCode == 96) {
                    audio[0].pause();
                    audio[0].currentTime = 0;
                    audio[0].play();
                }
                if (e.keyCode == 49 || e.keyCode == 97) {
                    audio[1].pause();
                    audio[1].currentTime = 0;
                    audio[1].play();
                }
                if (e.keyCode == 50 || e.keyCode == 98) {
                    audio[2].pause();
                    audio[2].currentTime = 0;
                    audio[2].play();
                }
                if (e.keyCode == 51 || e.keyCode == 99) {
                    audio[3].pause();
                    audio[3].currentTime = 0;
                    audio[3].play();
                }
                if (e.keyCode == 52 || e.keyCode == 100) {
                    audio[4].pause();
                    audio[4].currentTime = 0;
                    audio[4].play();
                }
                if (e.keyCode == 53 || e.keyCode == 101) {
                    audio[5].pause();
                    audio[5].currentTime = 0;
                    audio[5].play();
                }
                if (e.keyCode == 54 || e.keyCode == 102) {
                    audio[6].pause();
                    audio[6].currentTime = 0;
                    audio[6].play();
                }
                if (e.keyCode == 55 || e.keyCode == 103) {
                    audio[7].pause();
                    audio[7].currentTime = 0;
                    audio[7].play();
                }
                if (e.keyCode == 56 || e.keyCode == 104) {
                    audio[8].pause();
                    audio[8].currentTime = 0;
                    audio[8].play();
                }
                if (e.keyCode == 57 || e.keyCode == 105) {
                    audio[9].pause();
                    audio[9].currentTime = 0;
                    audio[9].play();
                }
            });
            $("#SectionStack_Intro_ActionsContainer_CallInput_Number").mask("000-000-000-000-000");
            $("#SectionStack_Intro_ActionsContainer_CallInput_Number").focus(function() {
                $("#SectionStack_Intro_ActionsContainer_CallInput").removeClass("Invalid");
                $("#SectionStack_Intro_ActionsContainer_CallInput_Number").attr("placeholder", $("#SectionStack_Intro_ActionsContainer_CallInput_Number").attr("data-placeholder")).val("");
                window.clearInterval(ValidateTimeout);
            });
            $("#SectionStack_Intro_ActionsContainer_CallInput_Button").click(function() {
                if ($.isNumeric($("#SectionStack_Intro_ActionsContainer_CallInput_Number").val().replace("-", "").replace("-", "").replace("-", "").replace("-", "")) && $("#SectionStack_Intro_ActionsContainer_CallInput_Number").val().length > 6) {
                    $(document).fullScreen(true);
                    Twilio();
                } else {
                    $("#SectionStack_Intro_ActionsContainer_CallInput").addClass("Invalid");
                    $("#SectionStack_Intro_ActionsContainer_CallInput_Number").attr("placeholder", $("#SectionStack_Intro_ActionsContainer_CallInput_Number").attr("data-placeholder2")).val("");
                    window.clearInterval(ValidateTimeout);
                    ValidateTimeout = setTimeout(function() {
                        window.clearInterval(ValidateTimeout);
                        $("#SectionStack_Intro_ActionsContainer_CallInput").removeClass("Invalid");
                        $("#SectionStack_Intro_ActionsContainer_CallInput_Number").attr("placeholder", $("#SectionStack_Intro_ActionsContainer_CallInput_Number").attr("data-placeholder")).val("");
                    }, 3000);
                }
                return false;
            });
            var CallInputIScroll = new IScroll('#SectionStack_Intro_ActionsContainer_CallInput_List', {
                mouseWheel: true,
                scrollbars: false,
                snap: 'li',
                click: true
            });
            $("#SectionStack_Intro_ActionsContainer_CallInput_Lang").click(function() {
                if ($("#SectionStack_Intro_ActionsContainer_CallInput_List").hasClass("Active") && !$("#SectionStack_Intro_ActionsContainer_CallInput_List").hasClass("Animating")) {
                    $.fn.fullpage.setAllowScrolling(true);
                    $("#SectionStack_Intro_Fader").fadeOut(300);
                    $("#SectionStack_Intro_ActionsContainer_CallInput_List").removeClass("Active");
                    $("#SectionStack_Intro_ActionsContainer_CallInput_List").addClass("Animating");
                    setTimeout(function() {
                        $("#SectionStack_Intro_ActionsContainer_CallInput_List").css("display", "none");
                        $("#SectionStack_Intro_ActionsContainer_CallInput_List").removeClass("Animating");
                    }, 300);
                } else {
                    $.fn.fullpage.setAllowScrolling(false);
                    $("#SectionStack_Intro_Fader").fadeIn(300);
                    $("#SectionStack_Intro_ActionsContainer_CallInput_List").addClass("Animating");
                    $("#SectionStack_Intro_ActionsContainer_CallInput_List").css("display", "block");
                    setTimeout(function() {
                        $("#SectionStack_Intro_ActionsContainer_CallInput_List").addClass("Active");
                        setTimeout(function() {
                            $("#SectionStack_Intro_ActionsContainer_CallInput_List").removeClass("Animating");
                        }, 300);
                        CallInputIScroll.refresh();
                    }, 50);
                }
                return false;
            });
            $("#SectionStack_Intro_ActionsContainer_CallInput_List").mouseenter(function() {
                $.fn.fullpage.setAllowScrolling(false);
            });
            $("#SectionStack_Intro_ActionsContainer_CallInput_List").mouseleave(function() {
                $.fn.fullpage.setAllowScrolling(true);
            });
            $("#SectionStack_Intro_ActionsContainer_CallInput_List li").click(function() {
                $("#SectionStack_Intro_ActionsContainer_CallInput_List li").removeClass("Active");
                $(this).addClass("Active");
                $("#SectionStack_Intro_ActionsContainer_CallInput_Lang > span").html($(this).attr("data-val"));
                $("#SectionStack_Intro_Fader").fadeOut(300);
                $("#SectionStack_Intro_ActionsContainer_CallInput_List").removeClass("Active");
                $("#SectionStack_Intro_ActionsContainer_CallInput_List").addClass("Animating");
                setTimeout(function() {
                    $("#SectionStack_Intro_ActionsContainer_CallInput_List").css("display", "none");
                    $("#SectionStack_Intro_ActionsContainer_CallInput_List").removeClass("Animating");
                }, 300);
            });
        }

        function Twilio() {
            var StandbyDetailEnded = false;
            var DiallingEnded = false;
            var TheBriefEnded = false;
            var SID = false;
            var TwilioInterval = false;
            $.fn.fullpage.setAllowScrolling(false);

            $("#TheMovie-StandbyDetail").unbind();
            $("#TheMovie-Dialling").unbind();
            $("#TheMovie-TheBrief").unbind();
            $("#TheMovie-TheBrief").get(0).pause();
            $("#TheMovie-TheBrief").get(0).currentTime = 0;
            $("#TheMovie-DiallingDetail").get(0).pause();
            $("#TheMovie-DiallingDetail").get(0).currentTime = 0;
            $("#TheMovie-Dialling").get(0).pause();
            $("#TheMovie-Dialling").get(0).currentTime = 0;
            $("#TheMovie-StandbyDetail").get(0).pause();
            $("#TheMovie-StandbyDetail").get(0).currentTime = 0;
            $(IntroAudio).animate({
                volume: 1
            });
            $("#Intro_Content,#Intro_Partners,#Intro_Lines,#Intro_FakeSVG, #HueeBurger, #fp-nav, #livechat-compact-container").animate({
                opacity: 0
            }, 300, function() {
                setTimeout(StepA, 1200);
            });

            function StepA() {
                $("#TheMovie-StandbyDetail").bind("ended", function() {
                    if (!StandbyDetailEnded) {
                        StandbyDetailEnded = true;
                        $("#TheMovie-Dialling").get(0).currentTime = 0;
                        $("#TheMovie-Dialling").get(0).play();
                        $("#TheMovie-Dialling").css("display", "block");
                        $("#TheMovie-StandbyDetail").css("display", "none");
                        $("#TheMovie-StandbyDetail").get(0).pause();
                        $("#TheMovie-StandbyDetail").get(0).currentTime = 0;
                        setTimeout(StepB, 9000);
                    }
                });
                $("#TheMovie-Dialling").bind("ended", function() {
                    if (!DiallingEnded) {
                        DiallingEnded = true;
                        $("#TheMovie-DiallingDetail").get(0).currentTime = 0;
                        $("#TheMovie-DiallingDetail").get(0).play();
                        $("#TheMovie-DiallingDetail").css("display", "block");
                        $("#TheMovie-Dialling").css("display", "none");
                        $("#TheMovie-Dialling").get(0).pause();
                        $("#TheMovie-Dialling").get(0).currentTime = 0;
                    }
                });
                $("#TheMovie-StandbyDetail").get(0).currentTime = 0;
                $("#TheMovie-StandbyDetail").get(0).play();
                $("#TheMovie-StandbyDetail").css("display", "block");
                $("#TheMovie").css("display", "block");
                $('.close-video-button').fadeOut();
            }

            function StepB() {
                var LANG = $('.SectionStack_Langs-pl.Active').text().toLowerCase();
                var CallNumber = $("#SectionStack_Intro_ActionsContainer_CallInput_List li.Active").attr("data-code") + $("#SectionStack_Intro_ActionsContainer_CallInput_Number").val().replace("-", "").replace("-", "").replace("-", "").replace("-", "");
                var TwilioPlayed = false;
                var AnswerThePhone = false;
                $.post("/_twilio/makecall.php?lang=" + LANG, {
                    numer: CallNumber
                }).done(function(data) {
                    SID = data;
                });
                TwilioInterval = setInterval(function() {
                    if (SID) {
                        $.post("/_twilio/status.php", {
                            SID: SID,
                            TIME: $("#TheMovie-TheBrief").get(0).currentTime
                        }, function(data) {
                            console.log('data', data);
                            if (data == 'initiated' && !AnswerThePhone) {
                                AnswerThePhone = true;
                                var showCloseBtnDelay = 6000
                                ShowCallUIElements(showCloseBtnDelay);
                            }
                            if (data == 'ringing' && !AnswerThePhone) {
                                AnswerThePhone = true;
                                ShowCallUIElements();
                            }
                            if (data == 'in-progress' && !TwilioPlayed) {
                                TwilioPlayed = true;
                                ShowCallUIElements(100);
                                setTimeout(StepC, 1330);
                            }
                            if (data == 'completed') {
                                if ($("#TheMovie-TheBrief").get(0).currentTime < 70) {
                                    clearInterval(TwilioInterval);
                                    EndVideo();
                                }
                            }
                            if (data == 'no-answer') {
                                if ($("#TheMovie-TheBrief").get(0).currentTime < 70) {
                                    clearInterval(TwilioInterval);
                                    EndVideo();
                                }
                            }
                            if (data == 'busy') {
                                clearInterval(TwilioInterval);
                                EndVideo();
                            }
                        });
                    }
                }, 500);
                BindCallUIActions(TwilioInterval);
            }

            function StepC() {
                $(IntroAudio).animate({
                    volume: 0.4
                });
                $("#TheMovie-DiallingDetail").get(0).pause();
                $("#TheMovie-TheBrief").get(0).currentTime = 0;
                $("#TheMovie-TheBrief").get(0).play();
                $("#TheMovie-TheBrief").css("display", "block");
                $("#TheMovie-DiallingDetail").css("display", "none");
                $("#TheMovie-DiallingDetail").get(0).pause();
                $("#TheMovie-DiallingDetail").get(0).currentTime = 0;
                $("#TheMovie-TheBrief").bind("ended", function() {
                    if (!TheBriefEnded) {
                        if (TwilioInterval) {
                            clearInterval(TwilioInterval);
                        }
                        TheBriefEnded = true;
                        EndVideo();
                    }
                });
            }

            function ShowCallUIElements(time) {
                var delay = time || 100;
                setTimeout(function () {
                    $('.close-video-button').css("display", "flex").hide().fadeIn();
                }, delay)

            }

            function BindCallUIActions(TwilioInterval) {

                $('.close-video-button').click(function() {

                    $.post("/_twilio/closeCall.php", { SID: SID }, function() {
                        if (data == 'completed') {
                            $('.close-video-button').fadeOut();
                            console.log('data - klik close', data);
                            // if ($("#TheMovie-TheBrief").get(0).currentTime < 70) {
                            //     clearInterval(TwilioInterval);
                            //     console.log('Połączenie zakończone');
                            //     EndVideo();
                            // }
                        }
                    });

                });

            }

            function EndVideo() {
                $("#TheMovie-StandbyDetail").unbind();
                $("#TheMovie-Dialling").unbind();
                $("#TheMovie-TheBrief").unbind();
                $("#TheMovie-Fade").animate({
                    opacity: 1
                }, 800, function() {
                    $(IntroAudio).animate({
                        volume: 0.4
                    });
                    $("#Intro_Content,#Intro_Partners,#Intro_FakeSVG, #fp-nav, #HueeBurger, #livechat-compact-container").animate({
                        opacity: 1
                    }, 300, function() {
                        $("#Intro_Lines").css("opacity", ".5");
                        $(document).fullScreen(false);
                        if ($('#HueeFullpage').length) {
                            $.fn.fullpage.setAllowScrolling(true);
                        }
                        SID = false;
                        StandbyDetailEnded = false;
                        DiallingEnded = false;
                        TheBriefEnded = false;
                        setTimeout(function() {
                            $("#TheMovie").animate({
                                opacity: 0
                            }, 300, function() {
                                $("#TheMovie-TheBrief").css("display", "none");
                                $("#TheMovie-TheBrief").get(0).pause();
                                $("#TheMovie-TheBrief")[0].currentTime = 0;
                                $("#TheMovie").css("display", "none").css("opacity", "1");
                                $("#TheMovie-Fade").css("opacity", "0");
                            });
                        }, 500);
                    });
                });
            }
        }
    }

    function PeopleSection() {
        if (!$("#Section-People").length) return false;
        $("#PeopleOwl").owlCarousel({
            items: 5,
            margin: 5,
            autoplay: false,
            nav: true,
            navText: ['', '']
        });

        function HeadshotPolygon() {
            var Polys = [5, 65, 10, 5, 85, 5, 95, 95];
            var Canv = Snap('#PeoplePolygon');
            var MaskGroup = Canv.group();
            var MaskBg = MaskGroup.rect(0, 0, 100, 100).attr({
                fill: '#FFF'
            });
            var Polygon = MaskGroup.polygon().attr({
                points: Polys,
                fill: '#000'
            });
            Canv.rect(0, 0, 100, 100).attr({
                fill: '#FFF',
                mask: MaskGroup
            });

            function Animation() {
                var NewPoly = [];
                $(Polys).each(function() {
                    NewPoly.push(this + Math.floor(Math.random() * 10 - 5));
                });
                if ($("#Section-People").hasClass("active") && AnimPerformance) {
                    Animations['people'] = Polygon.animate({
                        points: NewPoly
                    }, 5000, mina.easeinout, Animation);
                } else {
                    setTimeout(Animation, 1000)
                }
            }
            // Animation();
        }

        function IntroPolygon() {
            var Polys = [5, 5, 95, 35, 90, 70, 20, 95];
            var Canv = Snap('#SectionStack_People_ContentRow_Intro_Svg');
            var Polygon = Canv.polygon().attr({
                points: Polys,
                fill: '#e21e30'
            });

            function Animation() {
                var NewPoly = [];
                $(Polys).each(function() {
                    NewPoly.push(this + Math.floor(Math.random() * 10 - 5));
                });
                if ($("#Section-People").hasClass("active") && AnimPerformance) {
                    Animations['peopleb'] = Polygon.animate({
                        points: NewPoly
                    }, 5000, mina.easeinout, Animation);
                } else {
                    setTimeout(Animation, 1000)
                }
            }
            Animation();
        }
    }
    var NewsPage = function NewsPage() {
        $(".ArticleOwlCarousel").owlCarousel({
            items: 1,
            margin: 20,
            nav: true,
            navText: ['', ''],
            loop: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            autoplay: true
        });
    }

    ///////////// SCROLLBAR
    jQuery('.scrollbar-macosx').scrollbar();

    ///////////// ANIMATIONS
    var number = $(".vehicle-number");
    var img = $(".img-mtv, .img-airmax, .nofollow");
    var bigImg = $(".img-hendrix");
    $(document).on("mousemove",function(e) {
    var ax = -($(window).innerWidth()/2- e.pageX)/45;
    var ay = ($(window).innerHeight()/2- e.pageY)/35;
    var bx = -($(window).innerWidth()/2- e.pageX)/85;
    var by = ($(window).innerHeight()/2- e.pageY)/75;
    var cx = -($(window).innerWidth()/2- e.pageX)/80;
    var cy = ($(window).innerHeight()/2- e.pageY)/70;
    number.attr("style", "transform: rotateY("+ax+"deg) rotateX("+ay+"deg);-webkit-transform: rotateY("+ax+"deg) rotateX("+ay+"deg);-moz-transform: rotateY("+ax+"deg) rotateX("+ay+"deg)");
    img.attr("style", "transform: rotateY("+bx+"deg) rotateX("+by+"deg);-webkit-transform: rotateY("+bx+"deg) rotateX("+by+"deg);-moz-transform: rotateY("+bx+"deg) rotateX("+by+"deg)");
    bigImg.attr("style", "transform: rotateY("+cx+"deg) rotateX("+cy+"deg);-webkit-transform: rotateY("+cx+"deg) rotateX("+cy+"deg);-moz-transform: rotateY("+cx+"deg) rotateX("+cy+"deg)");
    });

    IntroSection();
    AboutSection();
    CapabilitiesSection();
    PeopleSection();
    AtentionSection();
    ChangeSection();
    NewsPage();
    Burger();
    YTKeepRatio();
    SectionStack_VideoOverlay();
    Mission();
    Share();
    LoadPopup();
});
